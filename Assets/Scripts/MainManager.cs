﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Security.Cryptography;

public class MainManager : MonoBehaviour
{
    [SerializeField] Text keyTxt;
    public static MainManager inst;

    public BinaryWriter binaryWriter;
    public BinaryReader binaryReader;

    [SerializeField] GameObject roomSettingsWindow;
    [SerializeField] GameObject presetsWindow;
    [SerializeField] GameObject appSettingsWindow;
    [SerializeField] GameObject setTemperModeWindow;
    [SerializeField] GameObject setLightModeWindow;
    [SerializeField] GameObject roomNameWindow;
    [SerializeField] GameObject connectionWindow;
    [SerializeField] GameObject roomButtonsBlock;
    [SerializeField] GameObject wrongPasswordWindow;
    [SerializeField] GameObject checkPasswordWindow;
    [SerializeField] GameObject errorWindow;

    [SerializeField] GameObject temperatureBlock;
    [SerializeField] GameObject lightningBlock;

    [SerializeField] Text errorTxt;

    [SerializeField] Button roomSettingsButton;
    [SerializeField] Button selectAllButton;
    [SerializeField] Button cancelSelectionButton;

    public List<Room> rooms;

    TcpClient tcpClient;
    NetworkStream networkStream;

    ICryptoTransform cryptoTransform;

    AesCryptoServiceProvider aesCryptoServiceProvider;
    RSACryptoServiceProvider rsaCryptoServiceProvider;

    AESKeyWrapper aesKeyWrapper;

    string ipAddress;
    int port;

    private void Awake()
    {
        inst = this;
        rooms = new List<Room>();

        aesCryptoServiceProvider = new AesCryptoServiceProvider();
        rsaCryptoServiceProvider = new RSACryptoServiceProvider();
    }

    void Start()
    {
        ReadXMLKey();
    }

    public void ShowRoomSettings()
    {
        temperatureBlock.SetActive(false);
        lightningBlock.SetActive(false);

        bool isLightningModeBlocked = true;
        bool isTemperatureModeBlocked = true;

        if (rooms.ToList().Count > 0)
        {
            roomSettingsWindow.SetActive(true);
        }
        else
            return;

        for (int i = 0; i < rooms.Count; i++)
        {
            if (rooms[i] != null)
            {
                if (rooms[i].lightningMode == Room.LightningMode.Manual)
                {
                    isLightningModeBlocked = false;
                }

                if (rooms[i].temperatureMode == Room.TemperatureMode.Manual)
                {
                    isTemperatureModeBlocked = false;
                }
            }
        }

        lightningBlock.SetActive(isLightningModeBlocked);
        temperatureBlock.SetActive(isTemperatureModeBlocked);
    }

    public void HideRoomSettingsWindow()
    {

        roomSettingsWindow.SetActive(false);
    }

    public void ShowPresetsWindow()
    {
        presetsWindow.SetActive(true);
    }

    public void HidePresetsWindow()
    {
        presetsWindow.SetActive(false);
    }

    public void ShowTemperatureModeWindow()
    {
        setTemperModeWindow.SetActive(true);
    }

    public void ShowLightningModeWindow()
    {
        setLightModeWindow.SetActive(true);
    }

    public void HideLightningModeWindow()
    {
        setLightModeWindow.SetActive(false);
    }

    public void HideTemperatureModeWindow()
    {
        setTemperModeWindow.SetActive(false);
    }

    public void ApplyLighningMode(int mode)
    {
        setLightModeWindow.SetActive(false);

        if (mode != 0)
            lightningBlock.SetActive(true);
        else
            lightningBlock.SetActive(false);

        for (int i = 0; i < rooms.Count; i++)
        {
            if (rooms[i] != null)
                rooms[i].SetLightningMode(mode);
        }
    }

    public void ApplyTemperatureMode(int mode)
    {
        setTemperModeWindow.SetActive(false);

        if (mode != 0)
            temperatureBlock.SetActive(true);
        else
            temperatureBlock.SetActive(false);

        for (int i = 0; i < rooms.Count; i++)
        {
            if (rooms[i] != null)
                rooms[i].SetTemperatureMode(mode);
        }
    }

    public void SetRoomsLightIntensity(Slider slider)
    {
        for (int i = 0; i < rooms.Count; i++)
        {
            if (rooms[i] != null)
            {
                rooms[i].SetLightIntensity(slider.value);
                rooms[i].SetLightningMode(0);
            }
        }
    }

    public void SetRoomsBatteryTemperature(Slider slider)
    {
        for (int i = 0; i < rooms.Count; i++)
        {
            if (rooms[i] != null)
            {
                rooms[i].SetBatteryTemperature(slider.value);
                rooms[i].SetTemperatureMode(0);
            }
        }
    }

    public void SetRoomsConditionerTemperature(Slider slider)
    {
        for (int i = 0; i < rooms.Count; i++)
        {
            if (rooms[i] != null)
                rooms[i].SetConditionerTemperature(slider.value);
        }
    }

    public void SetRoomCurtainsState(bool state)
    {
        for (int i = 0; i < rooms.Count; i++)
        {
            if (rooms[i] != null)
                rooms[i].SetCurtainsState();
        }
    }

    public void AddRoomToList(Room room)
    {
        rooms.Add(room);
        roomSettingsButton.interactable = true;
        cancelSelectionButton.interactable = true;
    }

    public void RemoveRoomFromList(Room room)
    {
        rooms.Remove(room);

        if (rooms.Count == 0)
        {
            roomSettingsButton.interactable = false;
            cancelSelectionButton.interactable = false;
        }
    }

    public void SetRoomName(InputField inputField)
    {
        for (int i = 0; i < rooms.Count; i++)
        {
            if (rooms[i] != null)
                rooms[i].SetRoomName(inputField.text);
        }
    }

    public void ShowRoomNameWindow()
    {
        roomNameWindow.SetActive(true);
    }

    public void HideRoomNameWindow()
    {
        roomNameWindow.SetActive(false);
    }

    public void ShowConnectionWindow()
    {
        connectionWindow.SetActive(true);
    }

    public void HideConnectionWindow()
    {
        connectionWindow.SetActive(false);
    }

    public void HideWrongPassWordWindow()
    {
        wrongPasswordWindow.SetActive(false);
    }

    public void CloseWindow(GameObject window)
    {
        window.SetActive(false);
    }

    public void OpenWindow(GameObject window)
    {
        window.SetActive(true);
    }

    public void SetIPAddress(InputField inputField)
    {
        ipAddress = inputField.text;
    }

    public void SetPort(InputField inputField)
    {
        port = System.Convert.ToInt32(inputField.text);
    }

    void ReadXMLKey()
    {
        try
        {
            string xmlStr = (Resources.Load<TextAsset>("XMLKey") as TextAsset).text;
            rsaCryptoServiceProvider.FromXmlString(xmlStr);
        }
        catch (System.Exception ex)
        {
            keyTxt.text = ex.ToString();
        }
    }

    public void ConnectToSever()
    {
        try
        {
            tcpClient = new TcpClient(ipAddress.ToString(), port);
            networkStream = tcpClient.GetStream();
            binaryWriter = new BinaryWriter(networkStream);
            binaryReader = new BinaryReader(networkStream);
            GetAESKeys();
            HideConnectionWindow();
            OpenWindow(checkPasswordWindow);
        }
      catch (System.Exception ex)
        {
            errorWindow.SetActive(true);
            keyTxt.text = ex.ToString();
        }
    }

    public string Encrypt(string str)
    {
        try
        {
            ICryptoTransform cryptoTransform = aesCryptoServiceProvider.CreateEncryptor();

            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, cryptoTransform, CryptoStreamMode.Write))
                using (StreamWriter sw = new StreamWriter(cs))
                    sw.WriteLine(str);
                return JsonUtility.ToJson(new BytesWrapper() { bytes = ms.ToArray() });
            }
        }
        catch (System.Exception ex)
        {
            keyTxt.text = ex.ToString();
            return null;
        }
    }

    public string Decrypt(string encStr)
    {
        try
        {
            ICryptoTransform cryptoTransform = aesCryptoServiceProvider.CreateDecryptor();

            using (MemoryStream ms = new MemoryStream(JsonUtility.FromJson<BytesWrapper>(encStr).bytes))
            {
                using (CryptoStream cs = new CryptoStream(ms, cryptoTransform, CryptoStreamMode.Read))
                using (StreamReader sr = new StreamReader(cs))
                    return sr.ReadLine();
            }
        }
        catch (System.Exception ex)
        {
            keyTxt.text = ex.ToString();
            return null;
        }
    }

    void GetAESKeys()
    {
        try
        {
            aesKeyWrapper = JsonUtility.FromJson<AESKeyWrapper>(binaryReader.ReadString());

            byte[] key = rsaCryptoServiceProvider.Decrypt(aesKeyWrapper.aesKey, false);
            byte[] iv = rsaCryptoServiceProvider.Decrypt(aesKeyWrapper.aesIV, false);

            aesCryptoServiceProvider.Key = key;
            aesCryptoServiceProvider.IV = iv;
        }
        catch (System.Exception ex)
        {
            keyTxt.text = ex.ToString();
        }
    }

    public void CheckPassword(InputField inputField)
    {
        try
        {
            binaryWriter.Write(Encrypt(inputField.text));
            binaryWriter.Flush();

            if (Decrypt(binaryReader.ReadString()) == "true")
            {
                roomButtonsBlock.SetActive(false);
                CloseWindow(checkPasswordWindow);
            }
            else
            {
                wrongPasswordWindow.SetActive(true);
                OpenWindow(wrongPasswordWindow);
            }
        }
        catch (System.Exception ex)
        {
            keyTxt.text = ex.ToString();
        }
    }

    public void WriteToClient(string str)
    {
        binaryWriter.Write(Encrypt(str));
        binaryWriter.Flush();
    }
}

[System.Serializable]  
public class DataWrapper
{
    public string methodName;
    public MethodParam methodParam;
}

[System.Serializable]
public class PresetData
{
    public int[] values;
}

[System.Serializable]
public class MethodParam
{
    public PresetData PresetData;

    public int roomIndex;

    public float lightIntensity;
    public float batteryValue;
    public float conditionerPower;

    public bool curtainsState;

    public Room.LightningMode lightMode;
    public Room.TemperatureMode temperatureMode;
}

[System.Serializable]
public class AESKeyWrapper
{
    public byte[] aesKey;
    public byte[] aesIV;
}

[System.Serializable]
public class BytesWrapper
{
    public byte[] bytes;
}