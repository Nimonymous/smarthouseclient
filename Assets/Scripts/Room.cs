﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Room : MonoBehaviour, IPointerClickHandler
{
    public enum LightningMode
    {
        Manual,
        Sensor,
        History
    }

    public enum TemperatureMode
    {
        Manual,
        Sensor,
        History
    }

    [SerializeField] bool isSelected;

    [SerializeField] int roomIndex;
    [SerializeField] string roomName;

    [SerializeField] bool curtainsState;

    [SerializeField] float lightIntensity;
    [SerializeField] float batteryTemperature;
    [SerializeField] float conditionerTemperature;

    [SerializeField] Text roomNameTxt;
    [SerializeField] Text lightIntensityTxt;
    [SerializeField] Text batteryTemperatureTxt;
    [SerializeField] Text conditionerTemperatureTxt;

    [SerializeField] Button roomButton;

    public LightningMode lightningMode;
    public TemperatureMode temperatureMode;

    private void Awake()
    {
        roomButton = GetComponent<Button>();

        lightIntensityTxt = transform.Find("Light").GetChild(0).GetComponent<Text>();
        batteryTemperatureTxt = transform.Find("Battery").GetChild(0).GetComponent<Text>();
        conditionerTemperatureTxt = transform.Find("Cond").GetChild(0).GetComponent<Text>();
        roomNameTxt = transform.Find("Name").GetChild(0).GetComponent<Text>();
    }

    private void Start()
    {
        RedrawData();
    }

    public void SelectButton()
    {
        MainManager.inst.AddRoomToList(this);
        isSelected = true;

        var colorBlock = roomButton.colors;
        colorBlock.normalColor = Color.red;
        roomButton.colors = colorBlock;
    }

    public void DeselectButton()
    {
        MainManager.inst.RemoveRoomFromList(this);
        isSelected = false;

        var colorBlock = roomButton.colors;
        colorBlock.normalColor = Color.white;
        roomButton.colors = colorBlock;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!isSelected)
            SelectButton();
        else
            DeselectButton();
    }

    public void RedrawData()
    {
        lightIntensityTxt.text = string.Concat("L", lightIntensity.ToString("0.0"));
        batteryTemperatureTxt.text = string.Concat("B", batteryTemperature.ToString("0.0"));
        conditionerTemperatureTxt.text = string.Concat("C", conditionerTemperature.ToString("0.0"));

        roomNameTxt.text = roomName;
    }

    public void SetLightIntensity(float intensity)
    {
        lightningMode = LightningMode.Manual;
        lightIntensity = intensity;
        RedrawData();
        DataWrapper wrapper = new DataWrapper();
        wrapper.methodName = "setroomlight";
        wrapper.methodParam = new MethodParam() { roomIndex = roomIndex, lightIntensity = intensity };

       MainManager.inst.WriteToClient(JsonUtility.ToJson(wrapper));
        
    }

    public void SetBatteryTemperature(float temperature)
    {
        temperatureMode = TemperatureMode.Manual;
        batteryTemperature = temperature;
        RedrawData();

        DataWrapper wrapper = new DataWrapper();
        wrapper.methodName = "setroombattery";
        wrapper.methodParam = new MethodParam() { roomIndex = roomIndex, batteryValue = temperature };

        MainManager.inst.WriteToClient(JsonUtility.ToJson(wrapper));
    }

    public void SetConditionerTemperature(float temperature)
    {
        conditionerTemperature = temperature;
        RedrawData();

        DataWrapper wrapper = new DataWrapper();
        wrapper.methodName = "setroomconditioner";
        wrapper.methodParam = new MethodParam() { roomIndex = roomIndex, conditionerPower = temperature };

        MainManager.inst.WriteToClient(JsonUtility.ToJson(wrapper));
    }

    public void SetRoomName(string name)
    {
        roomName = name;
        RedrawData();
    }

    public void SetCurtainsState()
    {
        curtainsState = !curtainsState;

        DataWrapper wrapper = new DataWrapper();
        wrapper.methodName = "switchroomcurtiansstate";
        wrapper.methodParam = new MethodParam() { roomIndex = roomIndex, curtainsState = curtainsState };

        MainManager.inst.WriteToClient(JsonUtility.ToJson(wrapper));
    }

    public void SetLightningMode(int mode)
    {
        lightningMode = (LightningMode)mode;


        if (lightningMode == LightningMode.History)
            lightIntensityTxt.text = "L(H)";
        else if (lightningMode == LightningMode.Sensor)
            lightIntensityTxt.text = "L(S)";
        else
            lightIntensityTxt.text = string.Concat("L", lightIntensity.ToString("0.0"));

        DataWrapper wrapper = new DataWrapper();
        wrapper.methodName = "setlightmode";
        wrapper.methodParam = new MethodParam() { roomIndex = roomIndex, lightMode = lightningMode };

        MainManager.inst.WriteToClient(JsonUtility.ToJson(wrapper));
    }

    public void SetTemperatureMode(int mode)
    {
        temperatureMode = (TemperatureMode)mode;

        if (temperatureMode == TemperatureMode.History)
        {
            batteryTemperatureTxt.text = "T(H)";
            conditionerTemperatureTxt.text = "T(H)";
        }
        else if (temperatureMode == TemperatureMode.Sensor)
        {
            batteryTemperatureTxt.text = "T(S)";
            conditionerTemperatureTxt.text = "T(H)";
        }
        else
        {
            batteryTemperatureTxt.text = string.Concat("B", batteryTemperature.ToString("0.0"));
            conditionerTemperatureTxt.text = string.Concat("C", conditionerTemperature.ToString("0.0"));
        }

        DataWrapper wrapper = new DataWrapper();
        wrapper.methodName = "settemperaturemode";
        wrapper.methodParam = new MethodParam() { roomIndex = roomIndex, temperatureMode = temperatureMode };

        MainManager.inst.WriteToClient(JsonUtility.ToJson(wrapper));
    }
}
